{-# LANGUAGE FlexibleInstances #-}

module Main where

import Control.Monad
import Data.IORef
import Replay
import System.Exit
import System.IO.Unsafe
import Test.QuickCheck hiding (Result) -- (Arbitrary,arbitrary,frequency,choose,generate,Gen)

-- | Runs the test suite for the replay library
main :: IO ()
main = do
  results <- runTests
  if and results
    then return ()
    else exitFailure

-- | Programs are parameterised over a 'tick' action.
--   Questions are () and answers are integers.
type Program = IO () -> Replay () Int Int

-- | A result is a pair of the final result of the program
--   and the number of 'ticks' executed.
type Result  = (Int, Int)
type Input   = [Int]

-- | A test case collects a program and a list of answers together
--   with its expected result.
data TestCase = TestCase
  { testName    :: String
  , testInput   :: Input
  , testResult  :: Result
  , testProgram :: Program
  }

listLongerThan :: Int -> Gen Input
listLongerThan n = replicateM n arbitrary

--TODO what happens here, every time same result
generateTestCase :: Int -> IO TestCase
generateTestCase n = do
  let name = "test" ++ show n
  nProgs <- sample' (arbitrary :: Gen IntAndReplay)
--  progs <- sample' (arbitrary :: Gen (Replay () Int Int))
  input <- sample' $ suchThat (arbitrary :: Gen Input) (not . null)
--  result <- sample' (arbitrary :: Gen Result)
  let inp = head input
      IaR n prog = head nProgs
      res = (n, 0)
--      prog = head progs

  putStrLn $ show inp
  putStrLn $ show res
  return TestCase
         { testName    = name,
           testInput   = inp, 
           testResult  = res,
           testProgram = \_ -> prog }

--TODO make it so that some do io tick and some don't
-- instance Arbitrary Program where
--   arbitrary = do
--     rep <- sample' (arbitrary :: Gen (Replay () Int Int))
--     return $ \tick -> do io tick
--                          head rep

data IntAndReplay = IaR {int :: Int, prog :: Replay () Int Int}

instance Arbitrary (Replay () Int Int) where
  arbitrary = do
    n <- choose (1,30) :: Gen Int
    case n of
         1 -> do x <- arbitrary 
                 return $ ((return x) >>= ask)
         2 -> do x <- arbitrary
                 return $ ask () >>= \_ -> (return x)
         10 -> do x <- arbitrary 
                  return $ (return x) >>= ask                 
         20 -> do x <- arbitrary
                  return $ ask () >>= \_ -> (return x)
         15 -> do x <- arbitrary 
                  return $ (return x) >>= ask                 
         25 -> do x <- arbitrary
                  return $ ask () >>= \_ -> (return x)
         _ -> return (return n)

instance Arbitrary IntAndReplay where
  arbitrary = do
    n <- choose (1,30) :: Gen Int
    case n of
         1 -> do x <- arbitrary  -- (arbitrary :: Gen (Replay () Int Int))
                 return $ IaR {int=n, prog= (return x >>= ask)}
         2 -> do x <- arbitrary
                 return $ IaR {int = n, prog = ask () >>= \_ -> (return x)}
         10 -> do x <- arbitrary 
                  return $ IaR {int = n, prog=(return x) >>= ask}
         20 -> do x <- arbitrary
                  return $  IaR {int = n, prog=ask () >>= \_ -> (return x)}
         15 -> do x <- arbitrary 
                  return $  IaR {int = n, prog=(return x) >>= ask}                
         25 -> do x <- arbitrary
                  return $  IaR {int = n, prog=ask () >>= \_ -> (return x)}
         _ -> return $ IaR {int=n, prog=return n}

--         3 -> return $ arbitrary >>= arbitrary --doesn't work




-- | Running a program.
runProgram :: Program -> Input -> IO Result
runProgram p inp = do
    counter <- newIORef 0
    let tick = modifyIORef counter (+1)
    x <- play (p tick) emptyTrace inp
    n <- readIORef counter
    return (x, n)
  where
    play prog t inp = do
      r <- run prog t
      case r of
        Right x      -> return x
        Left (_, t') -> case inp of
          []       -> error "too few inputs"
          a : inp' -> play prog (addAnswer t' a) inp'

-- | Checking a test case. Compares expected and actual results.
checkTestCase :: TestCase -> IO Bool
checkTestCase (TestCase name i r p) = do
  putStr $ name ++ ": "
  r' <- runProgram p i
  if r == r'
    then putStrLn "ok" >> return True
    else putStrLn ("FAIL: expected " ++ show r ++
                  " instead of " ++ show r')
         >> return False


-- | List of interesting test cases.
testCases :: [TestCase]
testCases =
  [ TestCase
    { testName    = "test1"
    , testInput   = [3,4]
    , testResult  = (8, 1)
    , testProgram = \tick -> do
        io tick
        a <- ask () -- should be 3
        b <- io (return 1)
        c <- ask () -- should be 4
        return (a + b + c)
    },

    TestCase
    { testName  = "test2"
    , testInput   = [0,0]
    , testResult  = (0, 2)
    , testProgram = \tick -> do
        io tick
        a <- ask () -- should be 0
        b <- io (return 0)
        c <- ask () -- should be 0
        io tick
        return (a + b + c)
    },
    unsafePerformIO $ generateTestCase 3,
    unsafePerformIO $ generateTestCase 4,
    unsafePerformIO $ generateTestCase 5,
    unsafePerformIO $ generateTestCase 6,
    unsafePerformIO $ generateTestCase 7,
    unsafePerformIO $ generateTestCase 8,
    unsafePerformIO $ generateTestCase 9,
    unsafePerformIO $ generateTestCase 10
  ]

-- | Running all the test cases.
runTests = mapM checkTestCase testCases

