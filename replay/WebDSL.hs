module WebDSL where
--(Web, Question, Answer) where


import Replay
import Data.Maybe
import Web.Scotty
import Data.Monoid
import Data.Text.Lazy (Text,pack,unpack)
import Control.Monad.IO.Class

--Types
--type Web a = Replay Question Question a
type Web a = Replay Question WAnswer a

data Question = Field Name (Maybe WAnswer)

type Name  = String
type Value = String
type WAnswer = Text

--Run function
runWeb :: Web Text -> ActionM ()
runWeb web = play [Result "()", Answer $ pack "foo"] --emptyTrace
 where
  play :: Trace WAnswer -> ActionM ()
  play t = do
    r <- liftIO $ run web t
    case r of
      Left (q,t') -> do
        liftIO $ putStr "Question: "
        let qName = getName q
        html $ page' [q]
--         p <- param qName
--        let answeredQ = answer q p
--        html $ page' [q, answeredQ]
        --- liftIO $ putStrLn $ unpack p
        --serve qAsText qName
--        r <- getLine
--        play (addAnswer t' q)
      Right x -> html x

  getQuestions :: [Question] -> Trace WAnswer -> [Question] --This is an answered Question!
  getQuestions (q:qs) (Result x:xs) = getQuestions (q:qs) xs
  getQuestions (q:qs) (Answer x:xs) = (answer q x) : (getQuestions qs xs)
  getQuestions _ _           = []

  answer :: Question -> WAnswer -> Question
  answer (Field name _) a = Field name (Just a)


serve :: Text -> Text -> ActionM ()
serve htmlcont paramname = do
   html htmlcont
   i <- getInput paramname
   html i

getValue :: Question -> Maybe WAnswer
getValue (Field name Nothing)  = Nothing
getValue (Field name (Just x)) = Just x

getName :: Question -> Text
getName (Field name _) = pack name

getInput :: Text -> ActionM Text
getInput paramname = param paramname `rescue` \ _ -> return (pack "")

instance Show Question where
  show = showField

showField (Field name (Just value)) = "<p><input type=\"text\" name=\"" ++ name ++
                                      "\" value=\"" ++ unpack value ++ "\" readonly></p>"
showField (Field name Nothing) = "<p><input type=\"text\" name=\"" ++ name ++ "\"></p>"

page' :: [Question] -> Text 
page' fields = 
  case any emptyValue fields of
      True  -> pack ("<html><body>\n" ++
            "<form method=get>\n" ++
             (unlines $ map show fields) ++
             "<input type=submit value=Submit>" ++
             "</form>\n" ++
             "</body></html>")
      False -> pack ("<html><body>\n" ++
            "<form method=get>\n" ++
             (unlines $ map show fields) ++
             "<input type=submit value=Submit>" ++
             "</form>\n" ++
             "</body></html>")
  where emptyValue (Field _ Nothing) = True
        emptyValue (Field _ _)       = False      

-- exampleF :: [Question]
-- exampleF = [Field "foo" (Just  "bar"),
--            Field "baz" Nothing,
--            Field "lol" (Just "cat")]

example :: Web Text
example = do
  io $ putStrLn "This is a test"
  fname <- ask $ mkQuestion "fname" []
  io $ putStrLn "Asked first name"
  lname <- ask $ mkQuestion "lname" []
  io $ putStrLn "Asked last name"
  return $ pack "Program ended!"

  where mkQuestion :: String -> String -> Question
        mkQuestion name []    = Field name Nothing
        mkQuestion name value = Field name (Just $ pack value)
