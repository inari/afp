{-# LANGUAGE OverloadedStrings #-}
module Main where

import WebDSL
import Web.Scotty
import Data.Monoid
import Data.Text.Lazy

main :: IO ()
main = scotty 3000 $ do
    get "/" (runWeb example)
    post "/" (runWeb example)
    -- get "/" serve
    -- post "/" serve
--
serve :: Text -> Text -> ActionM ()
serve htmlcont paramname = do
    html htmlcont
    i <- getInput paramname
    html i

--getInput :: Text -> ActionM Text
--getInput paramname = param paramname `rescue` \ _ -> return ""

page :: Text -> Text -> Text
page s s' = mconcat $
    [ "<html><body>"
    , "<p>Input1 was: ", s, "</p>"
    , "<p>Input2 was: ", s', "</p>"
    , "<form method=get>"
    , "<p>Type something here:</p>"
    , "<input name=text_input_id> <br />"
    , "<input name=text_input_id2>"
    , "<input type=submit value=OK>"
    , "</form>"
    , "</body></html>"
    ]
