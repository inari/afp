-- | An embedded DSL for drawing Turtle graphics
module Turtle (
  -- * The turtle type(s)
  Turtle(..)
  , Program(..)
  , Line(..)

  -- * Movement
  , forward, right
  , idle

  -- * Properties of the pen/screen
  , penup, pendown, color

  -- * Sequencing
  , (>*>), (<|>)


  -- * Derived operations
  , left, backward
  , limited, times, forever

  -- * Run functions
  , run
  , runTextual 
-- runGraphical defined in TurtleGraphics
  
  
  ) where

import TurtleUtils
import Graphics.HGL
import Data.Fixed
import Data.List

-- | Program is defined as basic operations Fwd, Rgt, Clr and Idle, 
-- | and they can be sequenced or run parallel
data Program = Seq Program Program | Par Program Program 
             | Fwd Double | Rgt Double | Clr Color | Idle 
   deriving (Show)

-- | Turtle is an external component to the program, updated after every command
data Turtle = Turtle Point Angle Color deriving (Show)

-- | A Line is a
data Line   = Line Point Point Color deriving (Show)

-- * Type signatures

right :: Double -> Program
forward :: Double -> Program
idle :: Program

penup :: Program
pendown :: Program
color :: Color -> Program

(>*>) :: Program -> Program -> Program
(<|>) :: Program -> Program -> Program

limited :: Int -> Program -> Program
times :: Int -> Program -> Program
forever :: Program -> Program

left :: Double -> Program
backward :: Double -> Program

runTextual :: Program -> IO ()

-- * Definitions

-- | Default turtle, starting from (0,0), 0 degrees and color black
defaultTurtle :: Turtle
defaultTurtle = (Turtle (0,0) 0.0 Black)

-- | Moves forward n pixels
forward n = Fwd n

-- | Turns right d degrees
right d = Rgt d

-- | Does nothing 
idle = Idle

-- | Stop drawing
penup = color White

-- | Start drawing
pendown = color Black

-- | Set the pen width
penwidth = undefined

-- | Changes the color of the turtle's pen
color c = Clr c


-- | Sequences programs
(>*>) p q = Seq p q 

-- | Parallelizes programs
(<|>) p q = Par p q


-- * Derived operations

-- | Makes the turtle stop after n commands
-- | Doesn't work with forever, because count expects a finite sequence
-- | TODO
limited n prg@(Seq p p') | count prg <= n = prg
                         | count p == n   = p
                         | count p >  n   = limited n p
                         | count p <  n   = p >*> limited m p'
   where m = n - (count p)
limited n (Par p p') = limited n p <|> limited n p' --parallel actions use up one unit of time
limited n cmd | n == 0    = idle
              | otherwise = cmd

count :: Program -> Int
count (Seq p p') = (count p) + (count p')
count (Par p p') = (count p) + (count p')
count _          = 1

-- | Repeats program n times
times n p | n <= 0    = idle
          | n == 1    = p
          | otherwise = p >*> times (n-1) p


-- | Repeats program forever
forever p = p >*> forever p


-- | Turns left n degrees
left n = right (360-n)

-- | Walks backward n steps; direction doesn't change
backward n = forward (-n)


-- * Run functions

-- | Takes a turtle and a program, and returns a tree of a list of lines.
-- | Lines can be interpreted textually or graphically.
run :: Turtle -> Program -> Tree Line Turtle    
run trtl@(Turtle pt dir clr) prg =
  case prg of 
    (Par p p') -> combine (run trtl p) (run trtl p')
    (Seq p p') -> applyOnLeaves (\trtl' -> run trtl' p') (run trtl p)
    Idle       -> leaf trtl
    (Clr c)    -> leaf (Turtle pt dir c)
    (Rgt d)    -> leaf (Turtle pt (orient dir d) clr)
    (Fwd n)    -> tree line trtl'
                    where newPt = (move trtl n)
                          trtl' = Turtle newPt dir clr
                          line = Line pt newPt clr

orient :: Angle -> Double -> Angle
orient dir d = (dir+d) `mod'` 360

move :: Turtle -> Double -> Point
move (Turtle pt@(x,y) dir clr) n = (newX, newY)
   where newX = x + (round $ n * (cos $ degreesToRadians dir))
         newY = y + (round $ n * (sin $ degreesToRadians dir))
     
  
-- | Textual run function, which prints the turtle's actions at each step in time.
runTextual prg = printLines "Turtle 1" $ bfs $ run defaultTurtle prg
      

-- | Helper function for runTextual.
-- | Note: this doesn't keep track of turtles, i.e. which turtle is which
-- | In parallel case it just outputs how many turtles are moving.
printLines :: String -> [[Line]] -> IO ()
printLines s ((line:[]):lines) = do
  putStrLn $ toString s line
  printLines s lines
printLines s (lines:morelines) = do
  let trtlStrs  = take (length lines) $ repeat "    Turtle "
      trtlCount = [1..length lines]
      turtles   = zipWith (\a b -> a ++ show b) trtlStrs trtlCount
      lines'    = zipWith (\t l -> toString t l) turtles lines
  mapM_ putStrLn lines'
  printLines "Turtle 1" morelines
printLines s [] = return ()

toString :: String -> Line -> String
toString [] (Line pt1 pt2 clr) = "Moved from " ++ show pt1 ++ " to " ++ show pt2
toString trtl (Line pt1 pt2 clr) = 
  trtl ++ " moved from " ++ show pt1 ++ " to " ++ show pt2