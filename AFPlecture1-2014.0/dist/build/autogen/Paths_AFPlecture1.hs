module Paths_AFPlecture1 (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [2014,0], versionTags = []}
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/inari/Library/Haskell/ghc-7.6.3/lib/AFPlecture1-2014.0/bin"
libdir     = "/Users/inari/Library/Haskell/ghc-7.6.3/lib/AFPlecture1-2014.0/lib"
datadir    = "/Users/inari/Library/Haskell/ghc-7.6.3/lib/AFPlecture1-2014.0/share"
libexecdir = "/Users/inari/Library/Haskell/ghc-7.6.3/lib/AFPlecture1-2014.0/libexec"
sysconfdir = "/Users/inari/Library/Haskell/ghc-7.6.3/lib/AFPlecture1-2014.0/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "AFPlecture1_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "AFPlecture1_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "AFPlecture1_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "AFPlecture1_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "AFPlecture1_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
