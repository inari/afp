-- | A graphical run function for the turtle DSL
module TurtleGraphics (runGraphical) where

import Turtle
import TurtleUtils
import Graphics.HGL
import Data.Monoid

defaultTurtle :: Turtle
defaultTurtle = Turtle (200,200) 0.0 Black

-- | Executes the Turtle program by performing the list of Graphics in sequence.
runGraphical :: Program -> IO ()
runGraphical prg = runGraphics $ do
  w <- openWindowEx "Turtle!" Nothing (500, 500) DoubleBuffered (Just 1000)
  drawInWindow w (polygon [(0,0),(0,500),(500,500),(500,0)])
  onTick w graphics
  getKey w >> return ()
  where tree = run defaultTurtle prg
        graphics = mapNested toGraphic $ bfs tree

onTick :: Window -> [[Graphic]] -> IO ()
onTick w []      = return ()
onTick w (xs:xxs)  = do
  getWindowTick w
  drawInWindow w $ overGraphics xs --All actions that are run in parallel
  onTick w xxs

-- | Helper function to convert Line to a Graphic
toGraphic :: Line -> Graphic
toGraphic (Line pt1 pt2 clr) = withColor clr $ line pt1 pt2

