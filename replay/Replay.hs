module Replay (
  Replay(..)
  , Trace(..)
  , Item(..)
  , io 
  , ask
  , emptyTrace
  , addAnswer
  , run
) where

import Data.Time
import System.IO
import Control.Monad.Trans

-- Types
data Replay q r a = 
  ReplayC { runReplay :: Trace r -> IO ( (Either q a),  Trace r, Trace r ) }
--  deriving (Show,Read)


type Trace r = [Item r]

data Item r = Answer r | Result String
  deriving (Show,Read)

-- Operations
instance Monad (Replay q r) where
  return x          = ReplayC $ \trace -> return $ (Right x, trace, [])
  --a minimal Replay computation: 
  --x as the return value, no changes to input trace, empty output trace.

  (ReplayC g) >>= f = ReplayC $ \trace -> do
     (result, inTrace, outTrace) <- g trace
     case result of 
       (Right a) -> do (result', inTrace', outTrace') <- runReplay (f a) inTrace 
                       return (result', inTrace, outTrace ++ outTrace')
        --applied g, got an answer. apply f to the answer, go to next round.

       (Left  q) -> return $ (Left q, inTrace, outTrace) 
        --applied g, got a question. return the question with the traces got so far.



io :: (Show a, Read a) => IO a -> Replay q r a
io ioA = ReplayC $ \trace -> 
     case trace of
         (Result t):ts -> return (Right $ read t, ts, [Result t]) 
         -- we get the result from the stack, don't perform the IO action.

         (Answer t):ts -> error "Expected Result, got Answer."
         -- value in trace is of wrong type, raise error

         []            -> do val <- ioA
                             return (Right val, [], [Result $ show val]) 
         -- we extract the value of the IO action, 
         -- return that as the `a' of the Replay monad, 
         -- and add a Result of the IO action as the output trace. 


ask :: q -> Replay q r r
ask q = ReplayC $ \trace -> 
     case trace of
         (Answer t):ts -> return (Right t, ts, [Answer t]) 
         -- we get the answer to the question from the stack.

         (Result t):ts -> error $ "Type of " ++ t ++ " mismatched. " ++
                                  "Expected Result, got Answer."
         -- value in trace is of wrong type, raise error

         []            -> return (Left q, [], [])
         -- no answers left in the stack, stop the program returning the question.

emptyTrace :: Trace r
emptyTrace = []

addAnswer  :: Trace r -> r -> Trace r
addAnswer t r = t ++ [Answer r]

run :: Replay q r a -> Trace r -> IO (Either (q, Trace r) a)
run (ReplayC f) trace = do
  (result, inTrace, outTrace) <- f trace
  case result of 
     (Right a) -> return $ Right a
     (Left  q) -> return $ Left (q, outTrace)

-- Example


example :: Replay String String Int
example = do
  t0 <- io getCurrentTime
  io (putStrLn "Hello!")
  age <- ask "What is your age?"
  io (putStrLn ("You are " ++ age))
  name <- ask "What is your name?"
  io (putStrLn (name ++ " is " ++ age ++ " years old"))
  t1 <- io getCurrentTime
  io (putStrLn ("Total time: " ++ show (diffUTCTime t1 t0)))
  return (read age)


running :: Replay String String a -> IO a
running prog = play emptyTrace
 where
  play t = do
    r <- run prog t    -- this is the same prog every time!
    case r of
      Left (q,t') -> do
        putStr ("Question: " ++ q ++ " ")
        r <- getLine
        play (addAnswer t' r)
      Right x -> return x


----

test = run example [Result "1164117157", Result "()", Answer "27", Result "()"]

{-
*Replay> test
Left ("What is your name?",[])
-}

test2 = run example [Result "1164117157", Result "()", Answer "27", Result "()", Answer "Starbuck"]
{-
*Replay> test2
Right 1
-}