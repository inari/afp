module Main where

import TurtleGraphics
import Turtle
import TurtleUtils
import Graphics.HGL

main = runGraphical coolExample 
 
testConstructs = do 
  putStrLn "Running square 3 times:"
  runTextual $ times 3 $ square 4

  putStrLn "\nLimiting a spiral to 15 moves:"
  runTextual $ limited 15 $ spiral 2 90

  putStrLn "\nRunning four squares parallel:"
  runTextual $ square 4 <|> square 4 <|> square 4 <|> square 4

  putStrLn "\nRunning a sawblade followed by four parallel squares:"
  runTextual $ sawblade 5 >*> (square 4 <|> square 4 <|> square 4 <|> square 4)
  
  putStr   "\nRunning a sawblade followed by two parallel squares, "
  putStrLn "of which one is followed by another square:"
  runTextual $ sawblade 5 >*> (square 4 <|> (square 4 >*> square 4))

  putStr   "\nTwo parallel squares followed by a sawblade, "
  putStrLn "to demonstrate the distributivity of the operators:"
  runTextual $ (square 4 >*> right 90 <|> square 4 >*> left 90) >*> sawblade 4

  putStrLn "\nGoing forward and backward, left and right:"
  runTextual $ forward 10 >*> right 90 >*> left 90 >*> backward 10 

  putStrLn "\nNext line runs colorful square forever, if you comment it out:"
  --runTextual foreverColors --comment out to test forever


-- First runs a normal spiral (colorful version!) and continues with an
-- infinite spiral from where the normal spiral ended.
pinkStar :: Program
pinkStar = colorfulSpiral 10 150 >*> color Magenta >*> spiralForever 100 150

sawtooth :: Program
sawtooth = right 45 >*> forward 56.56 >*> left 135 >*> forward 40 >*> right 90

sawblade :: Int -> Program
sawblade n = if n > 0 
               then sawtooth >*> right 30 >*> sawblade (n - 1)
               else idle

spiral :: Double -> Double -> Program
spiral size angle =
  if size < 100 
    then forward size >*> right angle >*> spiral (size + 2) angle 
    else idle

spiralForever :: Double -> Double -> Program
spiralForever size angle = forward size >*> right angle >*> spiralForever (size + 2) angle

colorfulSpiral :: Double -> Double -> Program
colorfulSpiral size angle = 
  if size < 100 
    then color c >*> forward size >*> right angle >*> colorfulSpiral (size + 2) angle 
    else idle
  where c = chooseClr size
        chooseClr :: Double -> Color
        chooseClr n 
                | n < 20  = Black
                | n < 40  = Blue
                | n < 60  = Cyan
                | n < 80  = Green
                | n < 100 = Yellow


coolExample :: Program
coolExample = (right 90 >*> penup >*> forward 60 >*> pendown >*> pinkStar) <|> (left 90 >*> penup >*> forward 60 >*> pendown >*> sawblade 12)

foreverColors :: Program
foreverColors = forever $ right 90 >*> colorfulSquare Blue Red 4 >*> colorfulSquare Green Magenta 4

square :: Int -> Program
square n = 
  if n > 0
    then forward 40 >*> right 90 >*> square (n-1)
    else idle

colorfulSquare :: Color -> Color -> Int -> Program
colorfulSquare c1 c2 n =
  if n > 0
   then if n `mod` 2 == 0
          then color1
          else color2
   else idle
   where color1 = color c1 >*> forward 40 >*> right 90 >*> colorfulSquare c1 c2 (n-1)
         color2 = color c2 >*> forward 40 >*> right 90 >*> colorfulSquare c1 c2 (n-1)