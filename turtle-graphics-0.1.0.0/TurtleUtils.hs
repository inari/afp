{-# LANGUAGE DeriveFunctor #-}
module TurtleUtils (
   Tree(..)
   , tree, leaf, insert, combine, applyOnLeaves
   , bfs, mapNested
   , degreesToRadians
   ) where

import Graphics.HGL

-- * Types that are used in Turtle and TurtleGraphics, but not imported outside

-- | A special tree for representing sequential or parallel operations.
-- | The type b is found in leaves, and is for the turtle.
data Tree a b = Leaf b
              | ParNode [Tree a b] -- (Tree a b) (Tree a b) --ParNode [Tree a b]
              | SeqNode a (Tree a b)
   deriving (Eq,Show,Functor)


-- * Helper functions for trees

isLeaf :: Tree a b -> Bool
isLeaf (Leaf _) = True
isLeaf _        = False

leaf :: b -> Tree a b
leaf b = Leaf b

tree :: a -> b -> Tree a b
tree x b = SeqNode x (Leaf b)

combine :: Tree a b -> Tree a b -> Tree a b
combine (ParNode children) t@(SeqNode _ _) = ParNode $ t : children
combine t@(SeqNode _ _) (ParNode children) = ParNode $ t : children
combine (ParNode children) (ParNode children') = ParNode $ children ++ children'
combine t                  t'              = ParNode [t,t']

insert :: (a,b) -> Tree a b -> Tree a b
insert (val,t) (SeqNode x (Leaf _)) = SeqNode x (SeqNode val (Leaf t))
insert (val,t) (SeqNode x next)     = SeqNode x (insert (val,t) next)
insert (val,t) (ParNode children)   = ParNode $ map (insert (val,t)) children


-- | A bind with arguments flipped. Our turtles are so nice and monadic :3
applyOnLeaves :: (b -> Tree a b) ->  Tree a b -> Tree a b
applyOnLeaves f tree =
  case tree of 
    (Leaf t)           -> f t
    (SeqNode val next) -> SeqNode val (applyOnLeaves f next)
    (ParNode children) -> ParNode (map (applyOnLeaves f) children)



bfs :: Tree a b -> [[a]]
bfs (SeqNode val (Leaf t)) = [[val]]
bfs (SeqNode val tree)     = [val] : (bfs tree)
bfs (ParNode children)     = toLists children

headTree :: Tree a b -> [a]
headTree (Leaf _)           = []
headTree (SeqNode val _)    = [val]
headTree (ParNode children) = concat $ map headTree children

tailTree :: Tree a b -> Tree a b
tailTree (Leaf b)           = Leaf b
tailTree (SeqNode val next) = next
tailTree (ParNode children) = ParNode (map tailTree children)

toLists :: [Tree a b] -> [[a]]
toLists ts | all isLeaf ts = []
           | otherwise  = (concat $ map headTree ts) : toLists (map tailTree ts)

-- * Other helper functions


degreesToRadians :: Double -> Double
degreesToRadians d = (pi * d) / 180

mapNested :: (a -> b) -> [[a]] -> [[b]]
mapNested f ([]:[])  = [[]]
mapNested f ([]:xxs) = [] : (mapNested f xxs)
mapNested f (xs:[])  = (map f xs) : []
mapNested f (xs:xxs) = (map f xs) : (mapNested f xxs)